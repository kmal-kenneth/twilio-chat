import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Chat",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Chat.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Login.vue"),
  },
  {
    path: "/management",
    name: "Management",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Management.vue"),
    beforeEnter: (to, from, next) => {
      let isAuthenticated = sessionStorage.getItem("isAuthenticated");

      if (to.name !== "Login" && !isAuthenticated) next({ name: "Login" });
      else next();
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
