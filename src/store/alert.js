export default {
  namespaced: true,
  state: () => ({
    alerts: [],
  }),
  mutations: {
    addAlert(state, payload) {
      state.alerts.push(payload);
    },

    deleteAlert(state, payload) {
      let index = state.alerts.findIndex(
        (alert) => alert.message === payload.message
      );
      state.alerts.splice(index, 1);
    },
  },
  actions: {
    async addAlert(context, payload) {
      if (payload.time) {
        context.commit("addAlert", payload.alert);

        setTimeout(() => {
          // this.deleteAlert(payload);
          context.dispatch("deleteAlert", payload);
        }, payload.time);
      } else {
        context.commit("addAlert", payload.alert);
      }
    },

    async deleteAlert(context, payload) {
      context.commit("deleteAlert", payload.alert);
    },
  },
  getters: {},
};
