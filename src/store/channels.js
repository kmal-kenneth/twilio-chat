import axios from "axios";

export default {
  namespaced: true,
  state: () => ({
    channels: [],
  }),
  mutations: {
    setChannels(state, channels) {
      state.channels = channels;
    },
    addChannel(state, channel) {
      state.channels.push(channel);
    },
    editChannel(state, payload) {
      let index = state.channels.findIndex(
        (channel) => channel.sid === payload.channel.sid
      );

      state.channels.splice(index, 1, payload.channel);
    },
    deleteChannel(state, payload) {
      let index = state.channels.findIndex(
        (channel) => channel.sid === payload.sid
      );
      console.log("deleted channel sid:" + payload.sid);
      state.channels.splice(index, 1);
    },
  },
  actions: {
    async getChannels(context) {
      let service =
        context.rootState.services.service == null
          ? JSON.parse(sessionStorage.getItem("service"))
          : context.rootState.services.service;

      await axios({
        method: "get",
        baseURL: context.rootState.baseUri,
        url: `/Services/${service.sid}/Channels`,
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          // JSON responses are automatically parsed.
          context.commit("setChannels", response.data.channels);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async addChannel(context, name) {
      let service =
        context.rootState.services.service == null
          ? JSON.parse(sessionStorage.getItem("service"))
          : context.rootState.services.service;

      var bodyFormData = new FormData();
      bodyFormData.set("FriendlyName", name);

      await axios({
        method: "post",
        baseURL: context.rootState.baseUri,
        url: `Services/${service.sid}/Channels`,
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          context.commit("addChannel", response.data);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async editChannel(context, payload) {
      let service =
        context.rootState.services.service == null
          ? JSON.parse(sessionStorage.getItem("service"))
          : context.rootState.services.service;

      var bodyFormData = new FormData();
      bodyFormData.set("FriendlyName", payload.name);

      await axios({
        method: "post",
        baseURL: context.rootState.baseUri,
        url: `Services/${service.sid}/Channels/${payload.sid}`,
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          context.commit("editChannel", {
            channel: response.data,
          });
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async deleteChannel(context, payload) {
      let service =
        context.rootState.services.service == null
          ? JSON.parse(sessionStorage.getItem("service"))
          : context.rootState.services.service;

      await axios({
        method: "delete",
        baseURL: context.rootState.baseUri,
        url: `Services/${service.sid}/Channels/${payload.sid}`,
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then(() => {
          context.commit("deleteChannel", {
            sid: payload.sid,
          });
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
  },
  getters: {
    orderedChanels(state) {
      return state.channels.sort((a, b) => {
        if (a.friendly_name < b.friendly_name) {
          return -1;
        }
        if (a.friendly_name > b.friendly_name) {
          return 1;
        }
        return 0;
      });
    },
    getChannelName(state, getters, rootState) {
      // eslint-disable-next-line no-debugger
      let index = state.channels.findIndex(
        (channel) => channel.sid === rootState.members.member.channel_sid
      );
      return state.channels[index].friendly_name;
    },
  },
};
