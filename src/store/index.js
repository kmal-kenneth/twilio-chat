import Vue from "vue";
import Vuex from "vuex";
import channels from "./channels";
import services from "./service";
import alert from "./alert";
import members from "./member";
import messages from "./messages";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ACCOUNT: "ACe5000b8f40f0944a92ec0b3681a4bdad",
    TOKEN: "329f939657b6e69f81b5b3cee8fafac5",
    baseUri: "https://chat.twilio.com/v2",
    alias: "",
  },
  mutations: {
    setAlias(state, payload) {
      state.alias = payload.alias;
    },
  },
  actions: {
    setAlias(context, payload) {
      context.commit("setAlias", payload);
    },
  },
  modules: {
    channels,
    services,
    alert,
    members,
    messages,
  },
});
