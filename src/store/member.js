import axios from "axios";
import { mapActions } from "vuex";

export default {
  namespaced: true,
  state: () => ({
    members: [],
    member: null,
    membersOnline: [],
  }),
  mutations: {
    setMember(state, payload) {
      state.member = payload;
    },
    setMembers(state, payload) {
      state.members = payload;
    },
    setMembersOnline(state, payload) {
      state.membersOnline = payload;
    },

    addMember(state, member) {
      state.members.push(member);
      state.member = member;

      sessionStorage.setItem("members", JSON.stringify(state.members));
    },
    // editMember(state, payload) {
    //   let index = state.members.findIndex(
    //     (member) => member.sid === payload.member.sid
    //   );

    //   state.members.splice(index, 1, payload.member);
    // },
    // deleteMember(state, payload) {
    //   let index = state.members.findIndex(
    //     (member) => member.sid === payload.sid
    //   );
    //   console.log("deleted member sid:" + payload.sid);
    //   state.members.splice(index, 1);
    // },
  },
  actions: {
    ...mapActions("alert", ["addAlert"]),

    async getMembers(context) {
      if (sessionStorage.getItem("members")) {
        context.commit(
          "setMembers",
          JSON.parse(sessionStorage.getItem("members"))
        );
      }
    },

    async getMember(context, payload) {
      if (context.state.members.length >= 1) {
        let index = context.state.members.findIndex(
          (member) =>
            member.channel_sid === payload.channel_sid &&
            member.identity === payload.identity
        );

        if (index >= 0) {
          await context.commit("setMember", context.state.members[index]);
        } else {
          console.log("member null");
          await context.commit("setMember", null);
        }
      } else {
        await context.commit("setMember", null);
      }
    },
    async getMembersOnline(context, payload) {
      // console.log(payload);
      await axios({
        method: "get",
        baseURL: context.rootState.baseUri,
        url: `/Services/${payload.service_sid}/Channels/${payload.channel_sid}/Members`,
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          // JSON responses are automatically parsed.
          context.commit("setMembersOnline", response.data.members);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async addMember(context, payload) {
      var bodyFormData = new FormData();
      bodyFormData.set("Identity", payload.identity);

      return axios({
        method: "post",
        baseURL: context.rootState.baseUri,
        url: `Services/${payload.service_sid}/Channels/${payload.channel_sid}/Members`,
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      }).then((response) => {
        context.commit("addMember", response.data);
      });
      // .catch((e) => {
      //   // console.log(e.message);
      // });
    },
  },
  getters: {
    orderedMembersOnline(state) {
      return state.membersOnline.sort((a, b) => {
        if (a.identity < b.identity) {
          return -1;
        }
        if (a.identity > b.identity) {
          return 1;
        }
        return 0;
      });
    },
  },
};
