import axios from "axios";

export default {
  namespaced: true,
  state: () => ({
    messages: [],
  }),
  mutations: {
    setMessages(state, payload) {
      if (state.messages.length != payload.length) {
        state.messages = payload;
      }
    },
    addMessage(state, payload) {
      state.messages.push(payload);
    },
  },
  actions: {
    async getMessages(context, payload) {
      await axios({
        method: "get",
        baseURL: context.rootState.baseUri,
        url: `/Services/${payload.service_sid}/Channels/${payload.channel_sid}/Messages`,
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          // JSON responses are automatically parsed.
          context.commit("setMessages", response.data.messages);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async addMessage(context, payload) {
      var bodyFormData = new FormData();
      bodyFormData.set("From", payload.identity);
      bodyFormData.set("Body", payload.body);

      await axios({
        method: "post",
        baseURL: context.rootState.baseUri,
        url: `Services/${payload.service_sid}/Channels/${payload.channel_sid}/Messages`,
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      }).then((response) => {
        context.commit("addMessage", response.data);
      });
    },
  },
};
