import axios from "axios";

export default {
  namespaced: true,
  state: () => ({
    service: null,
  }),
  mutations: {
    setService(state, service) {
      state.service = service;
      sessionStorage.setItem("service", JSON.stringify(service));
    },
  },
  actions: {
    async getServices(context) {
      await axios({
        method: "get",
        baseURL: context.rootState.baseUri,
        url: `/Services/`,
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          // JSON responses are automatically parsed.

          context.commit("setService", response.data.services[0]);
          console.log("sid got: " + response.data.services[0].sid);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
    async addService(context) {
      var bodyFormData = new FormData();
      bodyFormData.set("FriendlyName", "MyService");

      await axios({
        method: "post",
        baseURL: context.rootState.baseUri,
        url: `/Services`,
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" },
        auth: {
          username: context.rootState.ACCOUNT,
          password: context.rootState.TOKEN,
        },
      })
        .then((response) => {
          context.commit("setService", response.data);
          console.log("sid added: " + response.data.sid);
        })
        .catch((e) => {
          // this.errors.push(e);
          console.log(e);
        });
    },
  },
  getters: {},
};
